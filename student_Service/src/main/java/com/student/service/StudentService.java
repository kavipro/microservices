package com.student.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.student.pojo.Student;
import com.student.repository.UserRepo;


@Service
public class StudentService {

	@Autowired
	UserRepo repo;
	
	
	public List<Student> searchByTid(int tid)
	{
		return repo.getListByTid(tid);
	}
	
	public Student save(Student s) {
		return repo.save(s);
	}
	
	public HttpStatus delete(int s) {
		 repo.deleteById(s);
		 return HttpStatus.OK;
	}
	 
	public Optional<Student> search(int id) {
		return repo.findById(id);
	}
	
	public Boolean exists(int id) {
		return repo.existsById(id);
	}

	public List<Student> all(){
		return repo.findAll ();
	}
	
	public Student update(Student user,int id) {
		Student user1=new Student();
		user.setId(id);
		user1.setName(user.getName());
		user1.setRoll(user.getRoll());
		return repo.save(user);
	}
	
	public List<Student> getByIdAndTid(int id, int tid) {
		return repo.getStudentById(id, tid);
	}
}
