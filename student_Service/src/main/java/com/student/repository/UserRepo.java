package com.student.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.student.pojo.Student;
@Repository
public interface UserRepo extends JpaRepository<Student,Integer> {

	@Query("from Student where tid=?1")
	public List<Student> getListByTid(int tid);
	@Query("from Student where tid=?1")
	boolean searchByTid(int tid);
	@Query("from Student where id=?1 and tid=?2")
	List<Student> getStudentById(int id, int tid);
}
