package com.customer.entity;

public class CustomerDetails {

	private int id;
	private String name;
	private Vendor vendor;
	private Address address;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public CustomerDetails(int id, String name, Vendor vendor, Address address) {
		super();
		this.id = id;
		this.name = name;
		this.vendor = vendor;
		this.address = address;
	}
	public CustomerDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
