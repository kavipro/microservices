package com.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.customer.entity.Customer;
import com.customer.entity.CustomerDetails;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer>{

	Customer save(CustomerDetails details);

}
