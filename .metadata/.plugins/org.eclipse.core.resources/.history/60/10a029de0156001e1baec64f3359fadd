package com.student.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.student.pojo.Student;
import com.student.service.StudentService;

@RestController
public class HomeController {
	@Autowired
	StudentService service;
	
	@PostMapping("student")
	public ResponseEntity<Student> addStudent(@RequestBody Student s){
		if(s==null) {
			throw new RuntimeException("user Details are invalid");
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(s));
	}
	@DeleteMapping("student/{id}")
	public ResponseEntity<HttpStatus> deleteStu(@PathVariable int id){
		if(service.exists(id)) {
			return ResponseEntity.status(HttpStatus.OK).body(service.delete(id));
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
	@GetMapping("/student/{tid}")
	public ResponseEntity<List<Student>> search(@PathVariable int tid){
		List<Student> list=service.searchByTid(tid);
		if(list.isEmpty())
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();	
					
		return ResponseEntity.status(HttpStatus.OK).body(service.searchByTid(tid));
	}
	@GetMapping("student/")
	public List<Student> searchAll(){
		return service.all ();
	}
	@PutMapping("student/{id}")
	public ResponseEntity<Student> update(@PathVariable int id,@RequestBody Student user){
		if(service.exists(id))
		return ResponseEntity.status(HttpStatus.OK).body(service.update(user,id));
		else
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();	
	}
	@GetMapping("student/trainer/{tid}/{id}")
	public ResponseEntity<List<Student>> getStu(@PathVariable int tid, @PathVariable int id) {
		return ResponseEntity.status(HttpStatus.OK).body(service.getByIdAndTid(id, tid)); 
	}
}
