package com.address.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.address.entity.Address;
import java.util.List;

@Repository
public interface AddressRepo extends JpaRepository<Address, Integer>{

	Address findByAid(int aid);
}
