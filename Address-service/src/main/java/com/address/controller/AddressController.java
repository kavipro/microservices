package com.address.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.address.entity.Address;
import com.address.service.AddressService;

@RestController
public class AddressController {

	@Autowired
	AddressService service;
	@PostMapping("address")
	public ResponseEntity<Address> saving(@RequestBody Address address) {
		if(address==null) {
			throw new RuntimeException("Fields are empty");
		}
		return ResponseEntity.ok(service.add(address));
	}
	@GetMapping("address")
	public ResponseEntity<List<Address>> allAddresses(){
		return ResponseEntity.ok(service.showAddresses());
	}
	@GetMapping("address/{aid}")
	public ResponseEntity<Address> getOneAddess(@PathVariable int aid){
		return ResponseEntity.ok(service.fingOne(aid));
	}
}
