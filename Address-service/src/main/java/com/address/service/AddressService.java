package com.address.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.address.entity.Address;
import com.address.repository.AddressRepo;

@Service
public class AddressService {

	@Autowired
	AddressRepo repo;
	
	public Address add(Address address) {
		return repo.save(address);
	}
	public List<Address> showAddresses(){
		return repo.findAll();
	}
	public Address fingOne(int aid) {
		return repo.findByAid(aid);
	}
	
}
