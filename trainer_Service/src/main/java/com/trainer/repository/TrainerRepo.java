package com.trainer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.trainer.pojo.Trainer;
import java.util.Optional;



@Repository
public interface TrainerRepo extends JpaRepository<Trainer, Integer>{
	@Query("from Trainer where name=?1 and language=?2")
	Optional<Trainer>   findByNameAndLanguage(String name, String language);
	
//	Boolean  findByLanguage(String language);
}
