package com.trainer.pojo;

public class Student {

	private int id;
	private String name;
	private String roll;
	private int tid;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRoll() {
		return roll;
	}
	public void setRoll(String roll) {
		this.roll = roll;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public Student(int id, String name, String roll, int tid) {
		super();
		this.id = id;
		this.name = name;
		this.roll = roll;
		this.tid = tid;
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
