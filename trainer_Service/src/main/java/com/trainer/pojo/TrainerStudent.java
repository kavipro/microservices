package com.trainer.pojo;

import java.util.ArrayList;
import java.util.List;

public class TrainerStudent {

	private int tid;
	private String name;
	private String language;
	private String branch;
	private List<Student> student=new ArrayList<>();
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public List<Student> getStudent() {
		return student;
	}
	public void setStudent(List<Student> student) {
		this.student = student;
	}
	public TrainerStudent(int tid, String name, String language, String branch, List<Student> student) {
		super();
		this.tid = tid;
		this.name = name;
		this.language = language;
		this.branch = branch;
		this.student = student;
	}
	
}
