package com.trainer.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.trainer.pojo.Student;
import com.trainer.pojo.Trainer;
import com.trainer.pojo.TrainerStudent;
import com.trainer.service.TrainerService;


@RestController
public class HomeController {
	@Autowired
	TrainerService service;
	@PostMapping("/trainer")
	public ResponseEntity<?> addTrainer(@RequestBody Trainer trainer){
		if(trainer==null) {
			throw new  RuntimeException("user details are not present, unable to add");
		}
		if(service.findName(trainer.getName(), trainer.getLanguage()).isPresent()) {
			return ResponseEntity.ok("trainer already exists");
		}
		return ResponseEntity.status(HttpStatus.OK).body(service.addTrainerr(trainer));
	}
	@GetMapping("trainer/")
	public ResponseEntity<?> listOfAll(){
		List<Trainer> list=service.findAll();
		if(list.isEmpty()) {
			return ResponseEntity.ok("No Trainer Found");
		}
		return ResponseEntity.status(HttpStatus.OK).body(list);
	}
	
	@PostMapping("traineraddstudent")
	public Student addStudent(@RequestBody Student s)
	{
		return this.service.addStudent(s);
	}
	
	@GetMapping("/trainer/{tid}")
	public ResponseEntity<TrainerStudent> show(@PathVariable("tid") int tid)
	{
		System.out.println("method nvoked");
//		return this.service.getTrainerById(id);
		return ResponseEntity.ok( service.search(tid));
	}
	
}
