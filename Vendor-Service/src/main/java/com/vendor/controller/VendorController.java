package com.vendor.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.vendor.entity.Vendor;
import com.vendor.entity.VendorDetails;
import com.vendor.service.Service;

@RestController
public class VendorController {

	@Autowired
	Service service;
	@PostMapping("vendor")
	public ResponseEntity<Vendor> adding(@RequestBody Vendor vendor){
		return ResponseEntity.status(HttpStatus.OK).body(service.adding(vendor));
	}
	@GetMapping("vendor")
	public ResponseEntity<List<Vendor>> showing(){
		return ResponseEntity.ok(service.showing());
	}
	@GetMapping("vendor/{vid}")
	public ResponseEntity<VendorDetails> showingById(@PathVariable int vid){
		return ResponseEntity.ok(service.getSIngle(vid));
	}
	@PutMapping("vendor/{vid}")
	public ResponseEntity<Vendor> updating(@RequestBody Vendor vendor,@PathVariable int vid)
	{
		return ResponseEntity.ok(service.updateVendor(vendor,vid));
	}
}
