package com.vendor.entity;

public class VendorDetails {

	private int vid;
	private String shopName;
	private Address address;
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public VendorDetails(int vid, String shopName, Address address) {
		super();
		this.vid = vid;
		this.shopName = shopName;
		this.address = address;
	}
	
	
}
