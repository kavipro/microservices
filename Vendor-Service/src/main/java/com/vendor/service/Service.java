package com.vendor.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.vendor.entity.Address;
import com.vendor.entity.Vendor;
import com.vendor.entity.VendorDetails;
import com.vendor.repo.VendorRepo;

@org.springframework.stereotype.Service
public class Service {
	
	@Autowired
	RestTemplate template;

	@Autowired
	VendorRepo repo;
	
	public Vendor adding(Vendor vendor) {
		return repo.save(vendor);
	}
	public List<Vendor> showing(){
		return repo.findAll();
	}
	public VendorDetails getSIngle(int vid){
		Vendor vendor=repo.getById(vid);
		System.out.println(vendor.getShopName());
		Address address=template.getForObject("http://localhost:8007/address/"+vendor.getAid(), Address.class);
		VendorDetails details=new VendorDetails(vid, vendor.getShopName(),address);
		return details;
	}
	public Vendor updateVendor(Vendor vendor, int vid) {
		if(repo.existsById(vid))
		{vendor.setVid(vendor.getVid());
		return repo.save(vendor);}
		else {
			throw new RuntimeException("Vendoris not present");
		}
	}
}
