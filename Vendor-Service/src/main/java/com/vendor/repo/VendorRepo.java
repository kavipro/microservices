package com.vendor.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vendor.entity.Vendor;

@Repository
public interface VendorRepo extends JpaRepository<Vendor, Integer>{

}
